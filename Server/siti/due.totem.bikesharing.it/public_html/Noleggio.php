<?php
class Noleggio{
    
    private $codice;     
    private $dataInizio;          
    private $dataFine; 
    private $bici;
    private $utente;
    
    public function __construct($bici,$utente,$codice){
        $this->bici=$bici;
        $this->utente=$utente;
        $this->codice=$codice;
        $this->dataInizio=date("Y-m-d H:i:s");
    }
    public function setDataFine(){
        $this->dataFine=date("Y-m-d H:i:s");
    }
    
    public function aggiornaDB(){
        
    }
    function getCodice() {
        return $this->codice;
    }

    function getDataInizio() {
        return $this->dataInizio;
    }

    function getDataFine() {
        return $this->dataFine;
    }

    function getBici() {
        return $this->bici;
    }

    function getUtente() {
        return $this->utente;
    }
    public function __toString() {
        return  $this->codice.",'".$this->dataInizio."','',".$this->bici.",".$this->utente;
        
    }


}


?>