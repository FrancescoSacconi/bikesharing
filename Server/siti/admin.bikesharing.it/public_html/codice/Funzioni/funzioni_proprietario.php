<?php
class Proprietario{
                private $nome;
                private $cognome;
                private $username; 
                private $password;
                private $carta;
                private $email;
                
                public function __construct($nome,$cognome,$username,$password,$carta,$email){
                    $this->nome=$nome;
                    $this->cognome=$cognome;
                    $this->username=$username;
                    $this->password=$password;
                    $this->carta=$carta;
                    $this->email=$email;
                }
                function getNome() {
                    return $this->nome;
                }
                function getCognome() {
                    return $this->cognome;
                }
                function getUsername() {
                    return $this->username;
                }
                function getPassword() {
                    return $this->password;
                }
                function getCarta() {
                    return $this->carta;
                }
                function getEmail() {
                    return $this->email;
                }
                public function __toString(){
                   return  "'".$this->nome."','".$this->cognome."','".$this->username."','".$this->password."',".$this->carta.",'".$this->email."'";
                }

            }
?>