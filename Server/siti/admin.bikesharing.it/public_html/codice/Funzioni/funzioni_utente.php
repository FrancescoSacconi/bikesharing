<?php
class Utente{
                private $nome;
                private $cognome;
                private $username; 
                private $password;
                private $codiceCarta;
                private $email;
                private $saldo;
                private $isPropietario;
                
                public function __construct($nome,$cognome,$username,$password,$email,$isProprietario,$codiceCarta){
                    $this->nome=$nome;
                    $this->cognome=$cognome;
                    $this->username=$username;
                    $this->password=$password;
                    $this->email=$email;
                    $this->saldo=10;
                    $this->isPropietario=$isProprietario;
                    $this->codiceCarta=$codiceCarta;
                }
                function getNome() {
                    return $this->nome;
                }
                function getCognome() {
                    return $this->cognome;
                }
                function getUsername() {
                    return $this->username;
                }
                function getPassword() {
                    return $this->password;
                }
                function getCodiceCarta() {
                    return $this->codiceCarta;
                }
                function getEmail() {
                    return $this->email;
                }
                public function __toString(){
                   return  "'".$this->nome."','".$this->cognome."','".$this->username."','".$this->password."',".$this->codiceCarta.",'".$this->email."',".$this->isPropietario.",".$this->saldo;
                }

            }
?>