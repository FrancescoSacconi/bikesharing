<?php
    session_start();
    include 'funzioni_utente.php';
    // inclusione del file contenente la classe
    include "funzioni_mysql.php";
    // istanza della classe
    $data = new MysqlClass();
    // chiamata alla funzione di connessione
    $data->connetti();
    $username=htmlspecialchars($_POST['username']);
    $password=htmlspecialchars($_POST['password']);          
    // chiamata alla funzione per la rimozione
    $isUtente=$data->autenticaUTENTE($username,$password);
    if($isUtente){
        $_SESSION['update']=$username;
        header('location: ../update.php');
    }else{
        $_SESSION['risultato']=$username." non presente nel data base <br><br>";
        header('location:../scegliServizio.php');
    }
    // disconnessione
    $data->disconnetti();          
?>
}