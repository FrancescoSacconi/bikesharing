<?php

session_start();
if(isset($_SESSION['username'])&&isset($_SESSION['password'])){
if(isset($_SESSION['update'])&&$_SESSION['update']!=""){
include "funzioni_mysql.php";
$chiUpdate=$_SESSION['update'];
               
if(isset($_POST['USERNAME'])){
    $data = new MysqlClass();
    $data->connetti();
    $come=$_POST['come'];
    if($data->userValido(htmlspecialchars($come))){
        $data->update('USERNAME',$chiUpdate,$come);
        $data->disconnetti();
        $_SESSION['update']="";
    }else{
        $data->disconnetti();
        $_SESSION['risultato_update']='<er>Username gia in uso,non valida</er><br>';
        header('location:../update.php');
    }
}else if(isset($_POST['PASSWORD'])){
    $come=$_POST['come'];
    $data=new MysqlClass();
    $data->connetti();
    $data->update('PASSWORD',$chiUpdate,$come);
    $data->disconnetti();
    $_SESSION['update']="";
}else if(isset($_POST['EMAIL'])){
    $come=$_POST['come'];
    if(filter_var($come, FILTER_VALIDATE_EMAIL)){
        $data=new MysqlClass();
        $data->connetti();
        if($data->emailValido(htmlspecialchars($come))){
            $data->update('EMAIL',$chiUpdate,$come);
            $data->disconnetti();
            $_SESSION['update']="";
        }else{
            $data->disconnetti();
            $_SESSION['risultato_update']='<er>Email gia in uso,non valida</er><br>';
             header('location:../update.php');
        }
    }else{
            $_SESSION['risultato_update']='<td><er>Formato email non valido</er><br>';
            header('location:../update.php');
    }
}
}else{
    header("location:../scegliServizio.php");
    $_SESSION['update']="";
}
}else{
      header("location:../chiudiSessione.php");
      $_SESSION['update']="";
      
  }