<?php
class MysqlClass {
    // parametri per la connessione al database 
    private $nomehost = "localhost:3306";     
    private $nomeuser = "root";          
    private $password = "root"; //nel server root
    // nome del database da selezionare
    private $nomedb = "BIKESHARING";
    
    private $entrato=false;
    private $inserito=false;
    private $eliminato=false;
    
    private $query;
          
    // controllo sulle connessioni attive
    private $attiva = false;
 
    // funzione per la connessione a MySQL
    // funzione per la connessione a MySQL
    public function connetti(){
        if(!$this->attiva){
            if($connessione = mysql_connect($this->nomehost,$this->nomeuser,$this->password) or die (mysql_error())){
                // selezione del database
                $selezione = mysql_select_db($this->nomedb,$connessione) or die (mysql_error());
            }
        }else{
            return true;
        }
    }
    // funzione per la chiusura della connessione
    public function disconnetti(){
        if($this->attiva){
            if(mysql_close()){
                $this->attiva = false; 
                return true; 
            }else{
                return false; 
            }
        }
    }
    public function query($sql){
        if(isset($this->attiva)){
            $sql = mysql_query($sql) or die (mysql_error());
            return $sql;
        }else{
            return false; 
        }
    }
    //funzione per l'inserimento dei dati in tabella
    public function inserisci($t,$v,$r = null){
         if(isset($this->attiva)){
            $istruzione = 'INSERT INTO '.$t;
            if($r != null){
                $istruzione .= ' ('.$r.')';
            }
            for($i = 0; $i < count($v); $i++){
                if (is_string($v[$i])) {
                    $v[$i] = '"' . $v[$i] . '"';
                }
            }
            $v = implode(',',$v);
            $istruzione .= ' VALUES ('.$v.')';
 
            $query = mysql_query($istruzione) or die ("Inserimento non valido :".mysql_error());
            echo 'Inserimento avvenuto con successo';
 
        }else{
            return false;
        }
    }
    public function inserisciUtente($utente){
        $this->inserito=false;
         if(isset($this->attiva)){
            $istruzione = "INSERT INTO UTENTE (NOME,COGNOME,USERNAME,PASSWORD,CODICECARTA,EMAIL,ISPROPRIETARIO,SALDO) VALUES(".$utente.")";
            
            $query = mysql_query($istruzione); //or die ("Inserimento non valido :".mysql_error());
            var_dump($query);
           // echo 'Inserimento avvenuto con successo';
            if($query==true){
                if(mysql_affected_rows()==1){
                    $this->inserito=true;
                    $_SESSION['risultato_insert_utente']="Hai inserito l'utente: ".$utente->getUsername()." con successo<br><br>";
                    return true;
                }
            }
                $this->inserito=FALSE;
                $_SESSION['risultato_insert_utente']="Non sei riuscito a inserire l'utente: ".$utente->getUsername()." ".$istruzione."</br>Errore: ".mysql_error()."<br>";
                return FALSE;
        }else{
            return false;
        }
    }
    
    public function autenticaUTENTE($utente,$password){
        $this->entrato=false;
        if(isset($this->attiva)){
            $istruzione = "SELECT * FROM UTENTE WHERE USERNAME='".$utente."' AND PASSWORD='".$password."'";
            $query = mysql_query($istruzione) or die ("Errore: ".mysql_error());
            if(mysql_num_rows($query)==1){
                $this->entrato=true;
            }else{
            
                 return FALSE;
            }
            return true;
        }else{
          
            return false;
        }
    }
   
        public function inserisciBici($utente,$codice){
        $this->inserito=false;
         if(isset($this->attiva)){
            $istruzione = "INSERT INTO BICI (CODICE,PROPRIETARIO)  VALUES(".$codice.",".$utente.");";
            
            $query = mysql_query($istruzione); //or die ("Inserimento non valido :".mysql_error());
            var_dump($query);
           // echo 'Inserimento avvenuto con successo';
            if($query==true){
                if(mysql_affected_rows()==1){
                    $noleggio=  $this->ritornaNuovoCodiceNoleggio();
                    $istruzione = "INSERT INTO NOLEGGIO (CODICE,DATAINIZIO,BICI,CLIENTE) VALUES (".$noleggio.",NOW(),".$codice.",".$utente.")";
                    $query = mysql_query($istruzione); //or die ("Inserimento non valido :".mysql_error());
                    var_dump($query);
                    if($query==true){
                        if(mysql_affected_rows()==1){
                            $this->inserito=true;
                            $_SESSION['risultato_insert_bici']="La bici dell'utente con codice carta: ".$utente." ha come codice idenentificativo: ".$codice."<br>";
                    
                            return true;
                        }
                    }
                }
            }
                    
                $this->inserito=FALSE;
                $_SESSION['risultato_insert_bici']="Non sei riuscito a inserire la bici per l'utente codice carta: ".$utente." ".$istruzione."</br>Errore: ".mysql_error()."<br>Procedere manualmente";
               return false; 
        }else{
            return false;
        }
    }
    public function ritornaNuovoCodiceNoleggio(){
        if(isset($this->attiva)){
            $istruzione = 'SELECT * FROM NOLEGGIO ORDER BY CODICE DESC LIMIT 1';
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            return mysql_result($this->query,0,"CODICE")+1;
        }else{
            return false;
        }
    }
   
    public function ritornaNuovoCodiceBici(){
        if(isset($this->attiva)){
            $istruzione = 'SELECT * FROM BICI ORDER BY CODICE DESC LIMIT 1';
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            return mysql_result($this->query,0,"CODICE")+1;
        }else{
            return false;
        }
    }
   public function ritornaNuovoCodiceCarta(){
        if(isset($this->attiva)){
            $istruzione = 'SELECT * FROM UTENTE ORDER BY CODICECARTA DESC LIMIT 1';
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            return mysql_result($this->query,0,"CODICECARTA")+1;
        }else{
            return -1;
        }
    }
    
    public function autenticaAdmin($username,$password){
        $this->entrato=false;
        if(isset($this->attiva)){
            $istruzione = "SELECT * FROM UTENTE WHERE USERNAME='".$username."' AND PASSWORD='".$password."'";
            $query = mysql_query($istruzione) or die ("Errore: ".mysql_error());
            if(mysql_num_rows($query)==1){
                $this->entrato=true;
            }
        }else{
            return false;
        }
    }
    
    public function userValido($username){
        $this->entrato=false;
        if(isset($this->attiva)){
            $istruzione = "SELECT * FROM UTENTE WHERE USERNAME='".$username."'";
            $query = mysql_query($istruzione) or die ("Errore: ".mysql_error());
            return mysql_num_rows($query)==0;
        }else{
            return false;
        }
    }
    public function isProprietario($username){
        $this->entrato=false;
        if(isset($this->attiva)){
            $istruzione = "SELECT ISPROPRIETARIO FROM UTENTE WHERE USERNAME='".$username."'";
            $query = mysql_query($istruzione) or die ("Errore: ".mysql_error());
           if(mysql_num_rows($query)==1){   
               $res=mysql_result($query,0,"ISPROPRIETARIO");
               return $res==1;
           }
           return false;
        }else{
            return false;
        }
    }
    public function emailValido($email){
        $this->entrato=false;
        if(isset($this->attiva)){
            $istruzione = "SELECT * FROM UTENTE WHERE EMAIL='".$email."'";
            $query = mysql_query($istruzione) or die ("Errore: ".mysql_error());
            return mysql_num_rows($query)==0;
        }else{
            return false;
        }
    }
    function getEntrato() {
        return $this->entrato;
    }
    function getInserito() {
        return $this->inserito;
    }
    function getEliminato() {
        return $this->eliminato;
    }
    function rimuoviUtente($username,$password){
        $this->eliminato=false;
        $isPorprietario=  $this->isProprietario($username);
        
        if(isset($this->attiva)){//elimina tutti i noleggi
            try{
                
                mysql_query("START TRANSACTION");
                if($isPorprietario){
                    $bici=$this->ritornaBiciDiUtenteCarta($username);
                    $istruzione = "DELETE FROM NOLEGGIO WHERE BICI=".$bici;
                    $query = mysql_query($istruzione);
                }else{
                    $query=true;
                }
                if($query==true){
                    $istruzione = "DELETE FROM UTENTE WHERE USERNAME='".$username."' AND PASSWORD='".$password."'";
                    $query = mysql_query($istruzione);
                    if($query==true){
                        //sleep(30);
                        if(mysql_affected_rows()==1){
                            $this->eliminato=true;
                            $_SESSION['risultato']="Hai rimosso ".$username." con successo <BR>";
                            header('location:../scegliServizio.php');
                            mysql_query("COMMIT");
                            return;
                        }else if(mysql_affected_rows()==0){
                            $this->eliminato=false;
                            $_SESSION['risultato']=$username." non presente nel data base <br>";
                            header('location:../scegliServizio.php');
                            mysql_query("ROLLBACK");
                        }else{
                            $_SESSION['risultato']="Qualcosa è andato storto";
                            mysql_query("ROLLBACK");
                        }
                    }else{
                        $this->eliminato=FALSE;
                        $_SESSION['risultato']="Non sei riuscito a rimuovere ".$username."</br>Errore: ".mysql_error()." ".$istruzione;
                        header('location:../scegliServizio.php');
                        mysql_query("ROLLBACK");
                    }
                }else{
                    $_SESSION['risultato']="Non sei riuscito a rimuovere ".$username."</br>Errore: ".mysql_error()." ".$istruzione;
                    header('location:../scegliServizio.php');
                   mysql_query("ROLLBACK");
                }
           }catch (Exception $e){
                mysql_query("ROLLBACK");
           }
        }
    }
    public function ritornaTabella($tabella){
        if(isset($this->attiva)){
            if($tabella=='utente'||$tabella=='noleggio'){
                $istruzione = 'SELECT * FROM '.strtoupper($tabella);
                $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            }else if($tabella=='conto'){
                $istruzione = 'SELECT USERNAME,CODICE,TIMESTAMPDIFF(Hour, datainizio, datafine) AS ORE FROM UTENTE JOIN NOLEGGIO WHERE CODICECARTA=CLIENTE';
                $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            }
            
        }else{
            return false;
        }
    }
    function getQuery() {
        return $this->query;
    }
    
    function ritornaBiciDiUtente($utente){
        if(isset($this->attiva)){
            $istruzione = 'SELECT CODICE FROM BICI WHERE PROPRIETARIO='.$utente;
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            return mysql_result($this->query,'CODICE');
        }else{
            return false;
        }
    }function ritornaBiciDiUtenteCarta($utente){
        if(isset($this->attiva)){
            $istruzione = "SELECT CODICECARTA FROM UTENTE WHERE USERNAME='".$utente."'";
            $query= mysql_query($istruzione) or die ("Ritorna codice carta:accesso non valido :".mysql_error()." QUERY ".$istruzione);
            $codicecarta=mysql_result($query,0,'CODICECARTA');
            $istruzione = 'SELECT CODICE FROM BICI WHERE PROPRIETARIO='.$codicecarta;
            $query= mysql_query($istruzione) or die ("Ritorna codice bici:accesso non valido :".mysql_error()." QUERY ".$istruzione." UTENTE ".$utente);
            return mysql_result($query,0,'CODICE');
        }else{
            return false;
        }
    }
    
    function ritornaNoleggiDiBiciUtente($utente){
        if(isset($this->attiva)){
            $bici=$this->ritornaBiciDiUtente($utente);
            $istruzione = 'SELECT n.CODICE,TIMESTAMPDIFF(Hour, n.datainizio, n.datafine)+1 AS ORE FROM BICI b JOIN NOLEGGIO n WHERE b.CODICE=n.BICI n.BICI='.$bici;
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            
        }else{
            return false;
        }
    }
    function eliminaNoleggiIndividuali($utente){
        if(isset($this->attiva)){
            $bici=$this->ritornaBiciDiUtenteCarta($utente);
            $istruzione = "DELETE FROM NOLEGGIO WHERE BICI=".$bici." AND datafine IS NOT NULL";
            $query = mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            return $query;
        }else{
            return false;
        }
    }
    function eliminaNoleggiDiTutti(){
        if(isset($this->attiva)){
            $istruzione = "DELETE FROM NOLEGGIO WHERE datafine IS NOT NULL";
            $query = mysql_query($istruzione);
            return $query;
        }else{
            return false;
        }
    }
    function ritornaNoleggiDiTutti(){
        if(isset($this->attiva)){
            $istruzione = 'SELECT v.USER,v.EMAIL,sum(TIMESTAMPDIFF(Hour, n.datainizio, n.datafine)+1)AS ORE FROM NOLEGGIO n JOIN BICIUTENTI2 v WHERE n.BICI=v.BICI GROUP BY n.BICI';
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            
        }else{
            return false;
        }
    }
    function ritornaNoleggiIndividuale($utente){
        if(isset($this->attiva)){
            $istruzione = 'SELECT v.USER,v.EMAIL,sum(TIMESTAMPDIFF(Hour, n.datainizio, n.datafine)+1)AS ORE FROM NOLEGGIO n JOIN BICIUTENTI2 v WHERE n.BICI=v.BICI AND v.USER="'.$utente.'" GROUP BY n.BICI';
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            
        }else{
            return false;
        }
    }
    
    function update($cosa,$chi,$come){
        if(isset($this->attiva)){
            $istruzione = "UPDATE UTENTE SET ".$cosa."='".$come."' WHERE USERNAME='".$chi."'";
            $query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error()." ".$istruzione);
            
            $_SESSION['risultato']="Hai combiato ".$cosa." di ".$chi." in ".$come." <BR>";
            header('location:../scegliServizio.php');
        }else{
            $_SESSION['risultato']="Non sei riuscito a combiare ".$cosa." di ".$chi." in ".$come." <BR>";
            header('location:../scegliServizio.php');
            return false;
        }
    }

}
       
?>
