<?php session_start();
    include 'funzioni_utente.php';
    // inclusione del file contenente la classe
    include "funzioni_mysql.php";
    // istanza della classe
    $valida=true;
    if(isset($_POST['nome'])&&($_POST['nome']!=null)||$_POST['nome']!=''){
        $nome=htmlspecialchars($_POST['nome']);
        $_SESSION['nome_form']='';
    }else {
        $_SESSION['nome_form']='<tr><td></td><td><er>Nome non inserito</er></td></tr>';
        $valida=FALSE;
    }
    if(isset($_POST['cognome'])&&($_POST['cognome']!=null)||$_POST['cognome']!=''){
        $cognome=htmlspecialchars($_POST['cognome']);
        $_SESSION['cognome_form']='';
    }else {
        $_SESSION['cognome_form']='<tr><td></td><td><er>Cognome non inserito</er></td></tr>';
        $valida=FALSE;
    }
 
    if(isset($_POST['username'])&&($_POST['username']!=null)||$_POST['username']!=''){
        $data = new MysqlClass();
        // chiamata alla funzione di connessione
        $data->connetti();
        if($data->userValido(htmlspecialchars($_POST['username']))){
            $username=htmlspecialchars($_POST['username']);
            $_SESSION['username_form']='';
        }else{
            $valida=FALSE;
            $_SESSION['username_form']='<tr><td></td><td><er>Username non valido</er></td></tr>';
        }
        $data->disconnetti();
    }else {
        $valida=FALSE;
        $_SESSION['username_form']='<tr><td></td><td><er>Usename non inserioto</er></td></tr>';
    }
    if(isset($_POST['password'])&&($_POST['password']!=null)||$_POST['password']!=''){
        $password=htmlspecialchars($_POST['password']);
        $_SESSION['password_form']='';
    }else {
        $valida=FALSE;
        $_SESSION['password_form']='<tr><td></td><td><er>Password non inserita</er></td></tr>';
    }
    if(isset($_POST['ripeti_password'])&&($_POST['ripeti_password']!=null)||$_POST['ripeti_password']!=''){
        if($password==$_POST['ripeti_password']){
            $password=htmlspecialchars($_POST['ripeti_password']);
            $_SESSION['ripeti_password_form']='';
        }else{
            $_SESSION['ripeti_password_form']='<tr><td></td><td><er>Password diverse</er></td></tr>';
            $valida=FALSE;
        }
    }else {
        $valida=FALSE;
        $_SESSION['ripeti_password_form']='<tr><td></td><td><er>Conferma password non inserita</er></td></tr>';
    }
    if(isset($_POST['email'])&&($_POST['email']!=null)||$_POST['email']!=''){
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $_SESSION['email_form']='';
            $data->connetti();
            if($data->emailValido(htmlspecialchars($_POST['email']))){
                $email=htmlspecialchars($_POST['email']);
                $_SESSION['email_form']='';
            }else{
                $valida=FALSE;
                $_SESSION['email_form']='<tr><td></td><td><er>Email gia in uso non valido</er></td></tr>';
            }
            $data->disconnetti();
        }else{
            $valida=FALSE;
            $_SESSION['email_form']='<tr><td></td><td><er>Email non valida</er></td></tr>';
        }
    }else {
        $valida=FALSE;
        $_SESSION['email_form']='<tr><td></td><td><er>Email non inserita</er></td></tr>';
    }
    if(isset($_POST['radio'])&&($_POST['radio']!=null)||$_POST['radio']!=''){
        $_SESSION['proprietario']=$_POST['radio'];
        if($_POST['radio']=="SI"){
            $isproprietario=1;
            $_SESSION['proprietario']='';
        }else if($_POST['radio']=="NO"){
            $isproprietario=0;
            $_SESSION['proprietario']='';
        }
    }else {
        $valida=FALSE;
        $_SESSION['proprietario']='<tr><td></td><td><er>Proprietario non inserito</er></td></tr>';
    }
    
    if($valida==true){
        $data = new MysqlClass();
                            // chiamata alla funzione di connessione
        $data->connetti();
        $codiceCarta=$data->ritornaNuovoCodiceCarta();
        $utente=new Utente($nome,$cognome,$username,$password,$email,$isproprietario,$codiceCarta);
                                  // chiamata alla funzione per l'inserimento 
        $utente_inserito=$data->inserisciUtente($utente);
         
        if($isproprietario==1){
            $codiceBici=$data->ritornaNuovoCodiceBici();
            if($codiceBici!=-1){
                $data->inserisciBici($codiceCarta,$codiceBici);
            }
        }
        // disconnessione
        $data->disconnetti();
        header('location: ../scegliServizio.php');
    }else{
        
        header("location: ../inserisci.php");
    }
 ?>