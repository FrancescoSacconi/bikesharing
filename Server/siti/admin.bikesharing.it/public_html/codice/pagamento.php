<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Effettua pagamento</title>
        <style type="text/css">
            @import url("./css/Stile1.css");
            body{
                background-color: #e5e5e5;
                width: 1366px;
            };
        </style>
    </head>
    <body>
        <?php include './Funzioni/content.php';?>
        <div id="ins">
            
            <?php 
                session_start();
                include './Funzioni/funzioni_scegliServizio.php';
                if(isset($_SESSION['username'])&&isset($_SESSION['password'])){
                    $username= htmlspecialchars($_SESSION['username']);
                    $password= htmlspecialchars($_SESSION['password']);
                    autenticazione($username, $password); 
                }else{
                    header("location:../Funzioni/chiudiSessione.php");
                }
                if(isset($_SESSION['risultato_stampa_pagamenti'])&&$_SESSION['risultato_stampa_pagamenti']!=""){
                    echo $_SESSION['risultato_stampa_pagamenti'];
                    $_SESSION['risultato_stampa_pagamenti']="";
                }
               // echo "é entrato : ".$username."con la passwor : ".$password;
            ?>
            <h3>Come vuoi pagare?<h3>
            <form method="POST">
                <select name="richiesta">
                    <option value="individuale">Individuale</option>
                    <option value="totale">Totale</option>
                </select><br/>
                 Username:<input type="text" name="username" value="" />
                 <input type="submit" name="stampa" value="CONTINUA">
            </form>
            
            <?php
            //include './funzioni_mysql.php';
            
            include './Funzioni/funzioni_stampaTabella.php';
                if(isset($_POST['richiesta'])){
                    $data=new MysqlClass();
                    $data->connetti();
                    if($_POST['richiesta']=='totale'){ 
                        $data->ritornaNoleggiDiTutti();
                        $query=$data->getQuery();
                        stampaPagamentoDiTutti($query);
                    }else if($_POST['richiesta']=='individuale'){
                        if(isset($_POST['username'])&&$_POST['username']!=''){
                            $utente=$_POST['username'];
                            if($data->isProprietario($utente)){
                                $data->ritornaNoleggiIndividuale($utente);
                                $query=$data->getQuery();
                                stampaPagamentoIndividuale($query);
                            }else{
                                echo "Nome non valido oppure non è un proprietario.<br>";
                            }
                        }else{
                            echo "Nome non inserito.<br>";
                        }
                    }
                    
                    echo '</br></br>';
                    $data->disconnetti();
                }
            ?>

        </div>
    </body>
</html>
