<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Stato Data Base</title>
        <style type="text/css">
            @import url("./css/Stile1.css");
            body{
                background-color: #e5e5e5;
                width: 1366px;
            };
        </style>
    </head>
    <body>
        <?php include './Funzioni/content.php';?>
        <div id="ins">
            <?php 
                session_start();
                include './Funzioni/funzioni_scegliServizio.php';
                if(isset($_SESSION['username'])&&isset($_SESSION['password'])){
                    $username= htmlspecialchars($_SESSION['username']);
                    $password= htmlspecialchars($_SESSION['password']);
                    autenticazione($username, $password); 
                    if(isset($_SESSION['update'])||$_SESSION['update']!=""){
                        $chiUpdate=$_SESSION['update'];
                    }else{
                        header("location:./Funzioni/scegliServizio.php");
                    }
                    
                }else{
                    header("location:./Funzioni/chiudiSessione.php");
                }
                if(isset($_SESSION['risultato_update'])&&$_SESSION['risultato_update']!=''){
                    echo $_SESSION['risultato_update'];
                    $_SESSION['risultato_update']='';
                }
            ?>
            <h3>Cosa vuoi cambiare?<h3>
            <form method="POST">
                <select name="richiesta">
                    <option value="username">Username</option>
                    <option value="password">Password</option>
                    <option value="email">Email</option>
                </select>
                <input type="submit" name="stampa" value="CONTINUA">
            </form>
            <?php
            //include './funzioni_mysql.php';
                if(isset($_POST['richiesta'])&&isset($_SESSION['update'])){
                    $chiUpdate=$_SESSION['update'];
                    if($_POST['richiesta']=='username'){
                        ?>
                     <form action="./Funzioni/change.php" method="POST">
                        Nuovo Username: <input type="text" name="come" value="" />
                        <input type="submit" name="USERNAME" value="CONTINUA">
                     </form>
                    <?php
                    }else if($_POST['richiesta']=='password'){
                        ?>
                     <form action="./Funzioni/change.php" method="POST">
                        Nuova Password: <input type="text" name="come" value="" />
                        <input type="submit" name="PASSWORD" value="CONTINUA">
                        
                     </form>
                    <?php
                    }else if($_POST['richiesta']=='email'){
                        ?>
                     <form action="./Funzioni/change.php" method="POST">
                        Nuova email: <input type="text" name="come" value="" />
                        <input type="submit" name="EMAIL" value="CONTINUA">
                     </form>
                    <?php
                    }
                }
            ?>

        </div>
    </body>
</html>
