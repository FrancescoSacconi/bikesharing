<?php
session_start();
include 'funzioni_mysql.php';
include './Noleggio.php';
if(htmlspecialchars($_POST["op"])=="INDIETRO"){
    header("location: Home.php");
}else if(htmlspecialchars($_POST["op"])=="AVANTI"){
    $carta=htmlspecialchars($_POST["carta"]);
    if(is_numeric($carta)){
        $password=htmlspecialchars($_POST["password"]);
        /*TOTEM ADESSO é 1 MA PRIMA o POI VA CAMBIATO*/
        $totem=1;
        $data =new MysqlClass();
        $data->connetti();
        $data->autenticaUtente($carta, $password);
        $ok=$data->getEntrato();
        if($ok){
            $isProprietario=$data->isProprietario($carta);
            $controllaSaldo=$data->controllaSaldo($carta);
            if($controllaSaldo||$isProprietario){
                $numBiciSuff=$data->controlloPresenzaBici($totem);
                if($numBiciSuff){
                   $pin =$data->creaPreNoleggio($totem, $carta);
                   $_SESSION['pin']=$pin;
                }else{
                   $_SESSION['pin']="Bici attualmente non disponibili....";
                }
            }else{
                $_SESSION['pin']="Saldo non sufficiente per il noleggio...";
            }
        }
        header("location: ./StampaCodice.php");
        $data->disconnetti();
    }else{
        $_SESSION['autenticazione']="Autenticazione fallita<br> Carta o Pin errati";
        header("location: ./Ritira.php");
    }
}
    
?>