<?php
class MysqlClass{
    // parametri per la connessione al database 
    private $nomehost = "localhost:3306";     
    private $nomeuser = "root";          
    private $password = "root"; 
    // nome del database da selezionare
    private $nomedb = "BIKESHARING";
    
    private $entrato=false;
    private $inserito=false;
    private $eliminato=false;
    
    private $query;
          
    // controllo sulle connessioni attive
    private $attiva = false;
 
    // funzione per la connessione a MySQL
    // funzione per la connessione a MySQL
    public function connetti(){
        if(!$this->attiva){
            if($connessione = mysql_connect($this->nomehost,$this->nomeuser,$this->password) or die (mysql_error())){
                // selezione del database
                $selezione = mysql_SELECT_db($this->nomedb,$connessione) or die (mysql_error());
            }
        }else{
            return true;
        }
    }
    // funzione per la chiusura della connessione
    public function disconnetti(){
        if($this->attiva){
            if(mysql_close()){
                $this->attiva = false; 
                return true; 
            }else{
                return false; 
            }
        }
    }
    public function query($sql){
        if(isset($this->attiva)){
            $sql = mysql_query($sql) or die (mysql_error());
            return $sql;
        }else{
            return false; 
        }
    }
    //funzione per l'inserimento dei dati in tabella
    public function inserisci($t,$v,$r = null){
         if(isset($this->attiva)){
            $istruzione = 'INSERT INTO '.$t;
            if($r != null){
                $istruzione .= ' ('.$r.')';
            }
            for($i = 0; $i < count($v); $i++){
                if (is_string($v[$i])) {
                    $v[$i] = '"' . $v[$i] . '"';
                }
            }
            $v = implode(',',$v);
            $istruzione .= ' VALUES ('.$v.')';
 
            $query = mysql_query($istruzione) or die ("Inserimento non valido :".mysql_error());
            echo 'Inserimento avvenuto con successo';
 
        }else{
            return false;
        }
    }
    public function controllaSaldo($carta){
        if(isset($this->attiva)){
            $istruzione = 'SELECT SALDO FROM UTENTE WHERE CODICECARTA='.$carta;
            $query=mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            $resutl=mysql_result($query,0,"SALDO");
            return $resutl>=1;
        }else{
            return false;
        }
    }
    
    public function inserisciNoleggio($noleggio){
        session_start(); 
        $this->inserito=false;
         if(isset($this->attiva)){
            $istruzione = "INSERT INTO NOLEGGIO VALUES(".$noleggio.");";
            $query = mysql_query($istruzione); //or die ("Inserimento non valido :".mysql_error());
           // echo 'Inserimento avvenuto con successo';
            if($query==true){
                if(mysql_affected_rows()==1){
                    $this->inserito=true;
                   // $_SESSION['risultato']="Hai inserito ".$proprietario->getUsername()." con successo";
                    //header('location:scegliServizio.php');
                    return;
                }
            }
                $this->inserito=FALSE;
                //$_SESSION['risultato']="Non sei riuscito a inserire ".$proprietario->getUsername()."</br>Errore: ".mysql_error();
                //header('location:scegliServizio.php');
        }else{
            return false;
        }
    }
    public function autenticaUTENTE($carta,$password){
        $this->entrato=false;
        if(isset($this->attiva)){
            $istruzione = "SELECT * FROM UTENTE WHERE CODICECARTA=".$carta." AND PASSWORD='".$password."'";
            $query = mysql_query($istruzione) or die ("Errore: ".mysql_error());
            if(mysql_num_rows($query)==1){
                $this->entrato=true;
            }
        }else{
            return false;
        }
    }
    function getEntrato() {
        return $this->entrato;
    }
    function getInserito() {
        return $this->inserito;
    }
    function getEliminato() {
        return $this->eliminato;
    }
    public function creaPreNoleggio($totem,$UTENTE){
        if(isset($this->attiva)){
            $pin =rand(0,99999);
            if($pin<10){
                $pin="0000".$pin;
            }else if($pin>9&&$pin<100){
                $pin="000".$pin;
            }else if($pin>99&&$pin<1000){
                $pin="00".$pin;
            }else if($pin>999&&$pin<10000){
                $pin="0".$pin;
            }
            $istruzione = "INSERT INTO PRENOLEGGIO (CLIENTE,TOTEM,PINATTIVO) VALUES (".$UTENTE.",".$totem.",".$pin.")";
            $query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            var_dump($query); 
            
            if($query){
               return $pin;
            }  else {
                return false;
            }
            
        }else{
            return false;
        }
    }
    
    public function controlloPresenzaBici($totem){
        if(isset($this->attiva)){
            $istruzione = 'SELECT * FROM BICI WHERE TOTEM='.$totem;
            $query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            return mysql_num_rows($query)>=1;
        }else{
            return false;
        }
    }
    
    public function eliminaAutomaticamentePreNoleggi(){
        if(isset($this->attiva)){
            $date=date("Y-m-d H:i:s");
            $istruzione = "DELETE FROM PRENOLEGGIO WHERE UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(creato) > 60";
            $query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            
        }else{
            return false;
        }
        
    }

    public function aggiornaNoleggio($codiceNoleggio){
        if(isset($this->attiva)){
            $date=date("Y-m-d H:i:s");
            $istruzione = "UPDATE NOLEGGIO SET datafine='".$date."' WHERE CODICE=".$codiceNoleggio;
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            
        }else{
            return false;
        }
    }
    public function ritornaNuovoCodiceNoleggio(){
        if(isset($this->attiva)){
            $istruzione = 'SELECT * FROM NOLEGGIO ORDER BY CODICE DESC LIMIT 1';
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            return mysql_result($this->query,0,"CODICE");
        }else{
            return false;
        }
    }
    function getQuery() {
        return $this->query;
    }
    
    function ritornaNoleggiDiProprietario($proprietario){
        if(isset($this->attiva)){
            $istruzione = 'SELECT c.PROPRIETARIO,n.CODICE,n.datainizio,n.datafine FROM CONTO c,NOLEGGIO n WHERE c.PROPRIETARIO="'.$proprietario.'" AND c.NOLEGGIO=n.CODICE ORDER BY c.PROPRIETARIO';
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            
        }else{
            return false;
        }
    }
    function ritornaNoleggiDiTutti(){
        if(isset($this->attiva)){
            $istruzione = 'SELECT c.PROPRIETARIO, n.CODICE, n.datainizio, n.datafine from CONTO c,NOLEGGIO n WHERE  c.NOLEGGIO=n.CODICE order by c.PROPRIETARIO';
            $this->query= mysql_query($istruzione) or die ("accesso non valido :".mysql_error());
            
        }else{
            return false;
        }
    }
    public function isProprietario($carta){
        $this->entrato=false;
        if(isset($this->attiva)){
            $istruzione = "SELECT ISPROPRIETARIO FROM UTENTE WHERE CODICECARTA=".$carta."";
            $query = mysql_query($istruzione) or die ("Errore: ".mysql_error());
           if(mysql_num_rows($query)==1){   
               $res=mysql_result($query,0,"ISPROPRIETARIO");
               return $res==1;
           }
           return false;
        }else{
            return false;
        }
    }

}
       
?>
