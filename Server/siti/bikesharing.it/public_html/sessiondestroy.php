<?php
session_start();
// cancello tutti i dati di sessione
$_SESSION = array();
// distruggiamo la sessione
session_destroy();
header("location: iscriversi.php");
?>