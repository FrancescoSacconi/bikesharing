<html>  
    <head>
    <title>Bike Sharing</title>
        <style type="text/css">
            @import url("css/Stile1.css");
            body{
                background-color: #e5e5e5;
                width:1366px;
            };
        </style>
        <link rel="stylesheet" type="text/css" href="css/Stile1.css">
<meta name="viewport" content="width=device-width, user-scalable=no, 
        initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">    </head>
    <body>
        <?php include('template/banner.php')?>
	<?php session_start();
          if(isset($_SESSION['login'])){
            if($_SESSION['proprietario']==0)
                include('template/nav_logric.php');
            else include('template/nav_log.php');
        }
        else include('template/nav_main.php')?>
        <div id="ins"><img src="css/cicloturismo.jpg" width="200" height="200" style="float:left; margin:5px 5px 5px 5px; border-radius:10px 10px 10px 10px; border:3px solid #183870; "/>
            <b> Iscriversi </b></br>
        Il Bike Sharing prevede che siano installate delle stazioni in diversi punti della città dove collocare le biciclette. 
        Le biciclette sono bloccate e sono utilizzabili dopo averle sbloccate.
        Il servizio non è quindi generalmente usufruibile da tutti ma richiede una registrazione
        in questo modo si scoraggiano i furti poiché si è a conoscenza di chi ha utilizzato la bicicletta in quel momento.
        A seconda delle necessità, alla fine dell'utilizzo la bicicletta può essere riportata in un'altra stazione o nella medesima stazione di partenza.
        Le biciclette vengono prese in gestione da privati.
        Il pagamento è di 1 €/h ed il servizio è generalmente attivo 24 ore su 24.</br></br>
        <b>Come funziona?</b></br></br>
        <b>Affittare la propria bicicletta</b></br>
        <b>Step 1:</b> Andare nel nostro <a href="contatti.php">ufficio amministrativo</a> con la vostra bicicletta, li ci sarà un nostro impiegato che vi
        iscriverà con i vostri dati al nostro Server, preparerà la bicicletta ad essere affitata, e vi consegnerà una tessera prepagata che si caricherà da questo sito
        una volta <a href="login.php">loggati</a>.</br>
        <b>Step 2:</b> Ora potete attaccare la vostra bicicletta vicino uno degli appositi <a href="itotem.php">Totem</a> et voilà, il gioco è fatto.</br></br>
        Nel caso vorreste riprendere la vostra bicicletta basta inserire i vostri dati sul Totem e la bicicletta verrà sbloccata automaticamente.
        Se volete affittare un altra bicicletta, verrà applicato uno sconto sulla tassa iniziale, al posto di 1€/h verrà a costare 0.40€/h.
        </br></br>
        <b> Prendere una bicicletta </b></br>
        <b>Step 1:</b> Andare nel nostro <a href="contatti.php">ufficio amministrativo</a> dove ci sarà un nostro impiegato che vi iscriverà con i vostri dati
        nel nostro Server, e vi consegnerà una tessera prepagata che si caricherà da questo sito una volta <a href="login.php">loggati</a>. </br>
        <b>Step 2:</b> Una volta iscritti potrete andare direttamente in uno dei nostri <a href="itotem.php">Totem</a> a ritirare una bicicletta.
        
        
        </div>
        
    </body>
</html>
