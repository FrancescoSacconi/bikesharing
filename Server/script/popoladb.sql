USE BIKESHARING

INSERT INTO TOTEM VALUES(0001,'roma1');
INSERT INTO TOTEM VALUES(0002,'roma2');
INSERT INTO UTENTE VALUES('nome0','cognome0','user0','pas0',0,'email0@pr.it',1,10);
INSERT INTO UTENTE VALUES('nome1','cognome1','user1','pas1',1,'email1@pr.it',0,1);
INSERT INTO UTENTE VALUES('nome2','cognome2','user2','pas2',2,'email2@pr.it',1,0);
INSERT INTO UTENTE VALUES('nome3','cognome3','user3','pas3',3,'email3@pr.it',1,7);
INSERT INTO UTENTE VALUES('nome4','cognome4','user4','pas4',4,'email4@pr.it',0,20);
INSERT INTO UTENTE VALUES('nome5','cognome5','user5','pas5',5,'email5@pr.it',0,10);
INSERT INTO UTENTE VALUES('nome6','cognome6','user6','pas6',6,'email6@pr.it',0,12);
INSERT INTO UTENTE VALUES('nome7','cognome7','user7','pas7',7,'email7@pr.it',0,15);
INSERT INTO UTENTE VALUES('nome8','cognome8','user8','pas8',8,'email8@pr.it',0,16);
INSERT INTO UTENTE VALUES('nome9','cognome9','user9','pas9',9,'email9@pr.it',0,9);
INSERT INTO BICI VALUES(0,0,0001);
INSERT INTO BICI VALUES(1,2,0002);
INSERT INTO BICI VALUES(2,3,0001);